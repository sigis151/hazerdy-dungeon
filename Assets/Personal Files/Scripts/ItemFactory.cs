﻿/*  DAMAGE TYPES
 *  0: fire
 *  1: poison
 *  2: pierce
 *  3: blunt
 *  4: fall
 *  5: drown
 *  6: shock
 *  DON'T MESS UP THEIR ORDER!!!
 */

using UnityEngine;
public static class ItemFactory
{

    public static Item createItem(int id)
    {
        Object prefab = Resources.Load("prefabs/Item");
        Item temp = (GameObject.Instantiate(prefab) as GameObject).GetComponent("Item") as Item;
        switch (id)
        {
            case 0:
                temp.Set("Shield", new[]{ 0.0f, 0.0f, 1.0f, 0.95f, -0.1f, -0.1f, -0.1f });
                break;
            case 1:
                temp.Set("Leather boots", new[] { 0.3f, 0.0f, 0.0f, 0.0f, 0.35f, 0.0f, 0.0f });
                break;
            case 2:
                temp.Set("Sword", new[] { 0.0f, 0.0f, 0.0f, 0.0f, -0.2f, -0.25f, -0.6f });
                break;
            case 3:
                temp.Set("Stick", new[] { 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.3f, 0.0f });
                break;
            case 4:
                temp.Set("Wooden board", new[] { -0.5f, 0.0f, 0.7f, 0.45f, 0.0f, 1.0f, 0.0f });
                break;
            case 5:
                temp.Set("Wet raincoat", new[] { 0.95f, 0.3f, 0.2f, 0.1f, 0.0f, 0.0f, -1f });
                break;
            case 6:
                temp.Set("Bucket of water", new[] { 1.0f, 0.3f, 0.0f, 0.0f, 0.0f, 0.0f, -0.15f });
                break;
            case 7:
                temp.Set("Boots of speed", new[] { 0.4f, 0.4f, 1.4f, 0.4f, 0.4f, 0.4f, 0.4f });
                break;
            case 8:
                temp.Set("Metal boots", new[] { -0.25f, 0.05f, 0.0f, -0.15f, -0.1f, -0.15f, -0.3f });
                break;
            case 9:
                temp.Set("Umbrella", new[] { 0.0f, 0.0f, 0.4f, 0.6f, 0.8f, 0.0f, -0.55f });
                break;
            case 10:
                temp.Set("Adrenaline shot", new[] { 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f });
                break;
            case 11:
                temp.Set("Iron bar", new[] { -0.2f, 0.0f, 0.0f, 0.0f, -0.1f, -0.2f, -0.5f });
                break;
        }
        return temp;
    }

}