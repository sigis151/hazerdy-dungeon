﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;

	public class Level: MonoBehaviour
    {
		Player player;
        

        private Room roomLeft, roomRight;
        private Item itemLeft, itemRight;

        public void Start()
        {
			roomLeft = RoomSelectionPool.getRandom();
            roomLeft.transform.SetParent(gameObject.transform);
			roomRight = RoomSelectionPool.getRandom ();
            roomRight.transform.SetParent(gameObject.transform);
            RoomSelectionPool.resetLastPicked();
			int rand1 = UnityEngine.Random.Range(0, Constant.ITEM_COUNT);
			int rand2 = UnityEngine.Random.Range(0, Constant.ITEM_COUNT);
            placeItem(ItemFactory.createItem(rand1));
            placeItem(ItemFactory.createItem(rand2));

        }

        public void assignRoom(Room r)
        {
            if (roomLeft == null)
                roomLeft = r;
            else if (roomRight == null)
                roomRight = r;
            // TODO: randomize whether left or right is assigned first when picking from the pool.
        }

        public void resetRooms()
        {
            roomLeft = null;
            roomRight = null;
        }

        public void resetItems()
        {
            itemLeft = null;
            itemRight = null;
        }

        public void placeItem(Item i)
        {
            if (itemLeft == null)
                itemLeft = i;
            else if (itemRight == null)
                itemRight = i;
        }

        public Item pickUp(bool side) // BOOLEAN: 0 = LEFT, 1 = RIGHT
        {
            if (side)
                return itemRight;
            else
                return itemLeft;
        }
	/*
        public void reload()
        {
            resetItems();
            resetRooms();
            assignRoom(RoomSelectionPool.getRandom());
            assignRoom(RoomSelectionPool.getRandom());
            RoomSelectionPool.resetLastPicked();
			int rand1 = UnityEngine.Random.Range(0, Constant.ITEM_COUNT);
			int rand2 = UnityEngine.Random.Range(0, Constant.ITEM_COUNT);
			placeItem(ItemFactory.createItem(rand1));
			placeItem(ItemFactory.createItem(rand2));
        }
*/
        public void goThrough(bool side)
        {
            if (side)
            {
                Player.dealDamage(roomRight.getAbsoluteDamage() * Constant.DAMAGE_MULTIPLIER, roomRight.getDamageType());
                Player.addItem(pickUp(side));
            }
            else
            {
                Player.dealDamage(roomLeft.getAbsoluteDamage() * Constant.DAMAGE_MULTIPLIER, roomLeft.getDamageType());
                Player.addItem(pickUp(side));
            }
            if (Player.alive())
            {
                //reload();
            }
            else
            {
                Constant.doNothing(); // TODO: implement game-over action here
            }
        }
    }
