﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update  () {
		DetectTouch ();
	}
	/// <summary>
	/// Detects the side on which you tapped.
	/// </summary>
	public void DetectTouch()
	{
		if (Input.GetMouseButtonDown (0) && Input.mousePosition.y > 69f) {
			if(Input.mousePosition.x > Screen.width/2){
				//Go to right 
				Debug.Log("Right");
			}
			else{
				//Go to left
				Debug.Log("Left");
			}
		}
	}
}
