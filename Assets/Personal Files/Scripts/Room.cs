﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;
	public class Room: MonoBehaviour
    {
        public int damageType;
        public float damage;
        public string name;
		public Sprite sprite { get; private set;}

        public Room(string roomName, float dmg, int type)
        {

        }
		
		void Start()
		{

		}
		public void Set(string roomName, float dmg, int type){
		this.damage = dmg;
		this.damageType = type;
		this.name = roomName;
	}
	
	public float getAbsoluteDamage()
        {
            return this.damage * Constant.MAX_HEALTH;
        }

        public int getDamageType()
        {
            return this.damageType;
        }

        public string getName()
        {
            return this.name;
        }
    }