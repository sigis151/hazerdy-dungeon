﻿using UnityEngine;
using System.Collections;

public class ScoreManager
{
    private const string PREF_HIGH_SCORE = "score";
    private static ScoreManager instance;
    public static ScoreManager getInstance()
    {
        if (instance == null)
        {
            instance = new ScoreManager();
        }
        return instance;
    }
    
    public int score { get; private set; }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="lostHealth"></param>
    /// <param name="level"></param>
    /// <returns>is high score</returns>
    public bool calculateScore(int lostHealth, int level) // ggwp
    {
        score += (Constant.MAX_HEALTH - lostHealth) * level;
        if (score > PlayerPrefs.GetInt(PREF_HIGH_SCORE))
        {
            PlayerPrefs.SetInt(PREF_HIGH_SCORE, score);
            return true;
        }
        return false;
    }

    public void reset()
    {
        score = 0;
    }
}
